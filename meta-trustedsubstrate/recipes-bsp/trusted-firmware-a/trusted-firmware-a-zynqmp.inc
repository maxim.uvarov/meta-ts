# Xilinx kv260 and SOMs

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"
COMPATIBLE_MACHINE:zynqmp-zcu102 = "zynqmp-zcu102"

PV .= "+git${SRCREV_tfa}"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter := "${THISDIR}/files/zynqmp-kria-starter:"
FILESEXTRAPATHS:prepend:zynqmp-zcu102 := "${THISDIR}/files/zynqmp-zcu102:"

SRC_URI:append:zynqmp-kria-starter = " file://0001-zynqmp-fix-limit-BL31_LIMIT.patch"

TFA_DEBUG = "0"
TFA_UBOOT = "0"
#TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl31"
TFA_INSTALL_TARGET = "bl31"
# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD:zynqmp-zcu102 = ""
TFA_SPD:zynqmp-kria-starter = "opteed"

TFA_TARGET_PLATFORM = "zynqmp"

EXTRA_OEMAKE:append:zynqmp-kria-starter = " ZYNQMP_CONSOLE=cadence1 \
					    ZYNQMP_ATF_MEM_BASE=0xfffea000 \
					    ZYNQMP_ATF_MEM_SIZE=0x16000 \
					    RESET_TO_BL31=1 LOG_LEVEL=0"

EXTRA_OEMAKE:append:zynqmp-zcu102 = " RESET_TO_BL31=1 "

do_deploy:append() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
}

addtask deploy before do_build after do_compile
