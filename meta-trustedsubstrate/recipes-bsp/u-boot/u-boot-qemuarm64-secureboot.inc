# Temporarily disable capsule creation until upstream u-boot is fixed
#require u-boot-capsule.inc

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/${MACHINE}:"

SRC_URI += "\
    file://${MACHINE}.cfg \
    file://qemu/boot_opt.var \
    file://qemu/boot_order.var \
"

UBOOT_BOARDDIR = "${S}/board/emulation/qemu-arm"
UBOOT_ENV_NAME = "qemu-arm.env"
