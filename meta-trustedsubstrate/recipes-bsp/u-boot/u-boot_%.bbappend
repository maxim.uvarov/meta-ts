FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:${THISDIR}/u-boot/common:${THISDIR}/${PN}/${MACHINE}:"

# Always increment PR on u-boot config change or patches
PR .= "ts3"

# for tools/mkeficapsule
DEPENDS += "${@bb.utils.contains('MACHINE_FEATURES', 'capsule-update', 'gnutls-native', '', d)}"

# Add LWIP patches
SRCREV = "e4bd811ee8ca650209b93f79fb52583a3a1fca88"
SRC_URI = "git://github.com/muvarov/u-boot.git;protocol=https;branch=meta_ts_lwip_test_v10"

SRC_URI += "file://0002-fix-smbios-tables.patch"
SRC_URI += "file://0003-fix-authenticated-capsules.patch"
SRC_URI += "file://trs.env"
SRC_URI += "file://0001-event-Add-an-event-to-purge-DT-nodes-and-properties.patch"
SRC_URI += "file://0002-dt-Provide-a-way-to-remove-non-compliant-nodes-and-p.patch"
SRC_URI += "file://0003-fwu-Add-the-fwu-mdata-node-for-removal-from-devicetr.patch"
SRC_URI += "file://0004-capsule-Add-the-signature-node-for-removal-from-devi.patch"
SRC_URI += "file://0005-bootefi-Call-the-EVT_DT_NODE_PROP_PURGE-event-handle.patch"
SRC_URI += "file://0006-doc-Add-a-document-for-non-compliant-DT-node-propert.patch"
SRC_URI += "file://0007-test-event-Add-the-dt_purge-event-to-the-event-dump-.patch"
# lwip break, uses internel wget func SRC_URI += "file://0008-add-efi-https-boot.patch"
SRC_URI += "file://0001-efi_loader-Make-DisconnectController-follow-the-EFI-.patch"
SRC_URI += "file://${UEFI_CAPSULE_CERT_FILE};unpack=false"

SRC_URI += "file://http_boot.cfg"
SRC_URI += "file://common_opts.cfg"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'capsule-update', 'file://capsule_update.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'auth-capsule-update', 'file://auth_capsule_update.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'optee', 'file://optee.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "optee-ftpm", "file://optee-ftpm.cfg", "", d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "optee-stmm", "file://efi_vars_rpmb.cfg", "file://efi_vars_file.cfg", d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'secure-boot', 'file://secure_boot.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "file://tpm2.cfg", "", d)}"

do_configure:prepend() {
	cp -r ${WORKDIR}/*_defconfig ${S}/configs/ || true
}

do_configure:append() {
	if "${@bb.utils.contains('MACHINE_FEATURES', 'capsule-update', 'true', 'false', d)}"; then
		mkdir -p uefi_capsule_certs
		tar xpvfz "${WORKDIR}/${UEFI_CAPSULE_CERT_FILE}" -C uefi_capsule_certs
		# make sure it exists, in case paths or something changes
		test -f "${KCONFIG_CONFIG_ROOTDIR}/.config"
		echo CONFIG_EFI_CAPSULE_ESL_FILE=\"${B}/uefi_capsule_certs/CRT.esl\" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
		grep CONFIG_EFI_CAPSULE_ESL_FILE "${KCONFIG_CONFIG_ROOTDIR}/.config"
	fi

	if [ "${@bb.utils.contains('MACHINE_FEATURES', 'disable-console', '1', '0', d)}" = "1" ] ; then
		echo "CONFIG_BOOTMENU_DISABLE_UBOOT_CONSOLE=y" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
		echo "CONFIG_AUTOBOOT_MENU_SHOW=y" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
	fi

	if [ "${@bb.utils.contains('MACHINE_FEATURES', 'silence-console', '1', '0', d)}" = "0" ] ; then
		sed -i '/silent=1/d' ${WORKDIR}/trs.env
	fi

	cp ${WORKDIR}/trs.env ${UBOOT_BOARDDIR}/${UBOOT_ENV_NAME}
}

MACHINE_UBOOT_REQUIRE ?= ""

MACHINE_UBOOT_REQUIRE:rockpi4b = "u-boot-rockpi4b.inc"
MACHINE_UBOOT_REQUIRE:rpi4 = "u-boot-rpi4.inc"
MACHINE_UBOOT_REQUIRE:synquacer = "u-boot-synquacer.inc"
MACHINE_UBOOT_REQUIRE:tsqemuarm-secureboot = "u-boot-qemuarm-secureboot.inc"
MACHINE_UBOOT_REQUIRE:tsqemuarm64-secureboot = "u-boot-qemuarm64-secureboot.inc"
MACHINE_UBOOT_REQUIRE:zynqmp-kria-starter = "u-boot-zynqmp-kria-starter.inc"
MACHINE_UBOOT_REQUIRE:zynqmp-zcu102 = "u-boot-zynqmp-zcu102.inc"
MACHINE_UBOOT_REQUIRE:imx8mp-verdin = "u-boot-imx8mp-verdin.inc"

require ${MACHINE_UBOOT_REQUIRE}

require u-boot-certs.inc
